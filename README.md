# [USB C0nd0m](https://www.usbcondom.org/)

USB Condom prevents accidental data exchange when your device is plugged into a foreign computer/device or public charging station with a USB.

![usb condom scheme](images/Scheme.png)

## Usage example and features

1. Put [USB Condom](https://www.usbcondom.org/) to the ~~glory hole~~ USB port
2. Connect your device or your usb cable

* Full Data Protection (charge only mode)
* Data Transfer Indicator
* Anti-USB_killer feature

## Development setup

- Open KiCAD 5.0
- Work it Harder, Make it Better, Do it Faster, Makes Us Stronger 

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Extra

## Why do I need USB Condom? What problem does it solve?

There are a lot of different USB devices in the world that can put your phones, laptops, computers and any device with a USB interface in danger.

You can face such attacks as:

- **Juice jacking**. It is a type of cyber attack involving a charging port that doubles as a data connection. This often involves either installing malware or surreptitiously copying sensitive data from a smart phone, tablet, or other computer device.
- **BadUSB**. It is a type of hacker attack when a little USB stick firmware can be recognized as a keyboard, network adapter or anything else.
- **USB Killer**. It is an adapted device that can fry an entire device. Instead of a flash memory chip, its innards contain capacitors and a DC-DC converter that alters the voltage level of direct current. This is a deadly combination for your average USB port, along with anything attached to it.

## FAQ

Available here: https://www.usbcondom.org/faq

## What is [DC20e6](https://dc20e6.ru/)?

[DC20e6](https://dc20e6.ru/whoami) is a community for people interested in studying modern technologies and applying them in an unusual way which often implies information security and everything the positive meaning of the word "hacking" includes.
